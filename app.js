const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');

const logger = require('./src/helper/logger');
const authRouter = require('./src/routes/auth');
const signalRouter = require('./src/routes/signal');
const encountersRouter = require('./src/routes/encounters');

dotenv.config();

const app = express();

app.use(helmet());
app.use(morgan('dev'));
app.use(express.json());
app.use(mongoSanitize());
app.use(xss());

app.use('/api/v1/', authRouter);
app.use('/api/v1/', signalRouter);
app.use('/api/v1/', encountersRouter);

mongoose
  .connect(process.env.REMOTE_DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => {
    logger.info(`Connected to db`);
  });
app.listen(process.env.PORT, () => {
  logger.info(`Server restarted on port: ${process.env.PORT}`);
});
