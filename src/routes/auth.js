const { Router } = require('express');
const router = Router();
const authController = require('../controllers/auth');
const { check, validationResult } = require('express-validator');
const authHelper = require('../helper/auth');

router.post('/login', [check('udid').isString()], (req, res) => {
  authController
    .login(req.body.udid)
    .then(data => {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(422).json({
          message: 'fail',
          data: errors.array(),
        });
      }

      res.status(200).json({
        message: 'success',
        data,
      });
    })
    .catch(err => {
      res.status(500).json({
        message: 'fail',
        data: error,
      });
    });
});

router.patch('/login', authHelper.validateToken, (req, res) => {
  req.body.id = req.userID;
  authController
    .updateToken(req.body)
    .then(data => {
      res.status(200).json({
        message: 'success',
        data,
      });
    })
    .catch(err => {
      res.status(500).json({
        message: 'fail',
        data: error,
      });
    });
});
module.exports = router;
