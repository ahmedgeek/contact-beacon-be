const { Router } = require('express');
const router = Router();
const { check, validationResult } = require('express-validator');
const controller = require('../controllers/signals');
const authHelper = require('../helper/auth');

router.use(authHelper.validateToken);

router.post(
  '/signal',
  [check('latitude').isNumeric(), check('longitude').isNumeric()],
  (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({
        message: 'fail',
        data: errors.array(),
      });
    } else {
      req.body.userID = req.userID;
      controller
        .createSignal(req.body)
        .then(createdSignal => {
          return res.status(200).json({
            message: 'success',
            data: createdSignal,
          });
        })
        .catch(error => {
          return res.status(422).json({
            message: 'fail',
            data: error,
          });
        });
    }
  },
);

module.exports = router;
