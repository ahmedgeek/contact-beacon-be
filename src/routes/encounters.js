const { Router } = require('express');
const router = Router();
const encountersController = require('../controllers/encounters');
const { check, validationResult } = require('express-validator');
const authHelper = require('../helper/auth');

router.get('/encounters', authHelper.validateToken, (req, res) => {
  encountersController
    .list(req.userID)
    .then(data => {
      res.status(200).json({
        message: 'success',
        data,
      });
    })
    .catch(err => {
      res.status(500).json({
        message: 'fail',
        data: error,
      });
    });
});

router.post('/encounters', authHelper.validateToken, (req, res) => {
  encountersController
    .registerNewCase(req.userID)
    .then(data => {
      res.status(200).json({
        message: 'success',
        data,
      });
    })
    .catch(err => {
      res.status(500).json({
        message: 'fail',
        data: error,
      });
    });
});
module.exports = router;
