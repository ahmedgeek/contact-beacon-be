const { createLogger, format, transports } = require("winston");
const { combine, timestamp, printf } = format;

const customConsoleFormat = printf(({ level, message }) => {
  return `${level} -- ${message}`;
});

const logger = createLogger({
  level: "info",
  format: format.json(),
  defaultMeta: { service: "server" },
  transports: [
    new transports.File({
      filename: "../logs/error.log",
      level: "error"
    }),
    new transports.File({ filename: "../logs/combined.log" }),
    new transports.Console({
      level: "info",
      format: combine(format.colorize(), timestamp(), customConsoleFormat)
    })
  ]
});
module.exports = logger;
