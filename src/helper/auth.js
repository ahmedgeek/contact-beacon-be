const jwt = require('jsonwebtoken');

exports.validateToken = (req, res, next) => {
  const bearerHeader = req.headers['authorization'];

  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];

    jwt.verify(
      bearerToken,
      process.env.WEB_TOKEN_SECRET,
      (error, decoded) => {
        if (error) {
          return res.sendStatus(403);
        } else {
          req.userID = decoded.id;
          next();
        }
      },
    );
  } else {
    res.sendStatus(403);
  }
};
