var apn = require('apn');
var User = require('../models/user');

var options = {
  cert: __dirname + '/../../push/cert.pem',
  key: __dirname + '/../../push/key.pem',
  production: false,
};

var apnProvider = new apn.Provider(options);

exports.sendNotification = function(userID, payload) {
  User.findById(userID, function(err, user) {
    if (err) {
      return next(err);
    }

    var token = user.deviceToken;

    var options = {
      cert: __dirname + '/../../push/cert.pem',
      key: __dirname + '/../../push/key.pem',
    };

    var note = new apn.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600;
    note.badge = 3;
    note.sound = 'ping.aiff';
    note.alert =
      'You made contact with a confirmed COVID-19 case.\nOpen to see more details.';
    note.payload = payload;
    apnProvider.send(note, token);
  });
};
