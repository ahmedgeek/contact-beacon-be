const jwt = require('jsonwebtoken');
const encounterModel = require('../models/encounter');
const signalModel = require('../models/signal');
const encountersModel = require('../models/encounter');
const logger = require('../helper/logger');
const notifications = require('../helper/push');
const encounters = {};

encounters.list = encounteredID => {
  const query = encounterModel.find({
    encounteredID,
  });

  return query.exec();
};

encounters.registerNewCase = confirmedCaseID => {
  //get all locations in past 9 days
  const query = signalModel.find({
    userID: confirmedCaseID,
  });

  return query
    .exec()
    .then(docs => {
      const encounters = [];
      let counter = 1;
      const processingPromise = new Promise((resolve, reject) => {
        for (const doc of docs) {
          const encounterQuery = signalModel.find({
            location: {
              $near: {
                $maxDistance: 10,
                $geometry: {
                  type: 'Point',
                  coordinates: [
                    doc.location.coordinates[0],
                    doc.location.coordinates[1],
                  ],
                },
              },
            },
            userID: { $ne: doc.userID },
          });

          encounterQuery.exec().then(actualEncounters => {
            actualEncounters.forEach(item => {
              encounters.push(item);
            });

            counter++;
            if (counter === docs.length) {
              var uniqueArray = [];
              encounters.forEach(function(item) {
                var i = uniqueArray.findIndex(
                  x => x.userID == item.userID,
                );
                if (i <= -1) {
                  uniqueArray.push(item);
                }
              });

              uniqueArray.forEach(item => {
                const payload = {
                  confirmedCaseID: confirmedCaseID,
                  encounteredID: item.userID,
                  location: {
                    coordinates: [
                      item.location.coordinates[1],
                      item.location.coordinates[0],
                    ],
                  },
                  encounterDate: item.createdAt,
                };
                encountersModel.create(payload);

                notifications.sendNotification(item.userID, {
                  encounterDate: item.createdAt,
                });
              });
              resolve(uniqueArray);
            }
          });
        }
      });

      return processingPromise;
    })
    .catch(error => {
      console.log(error);
    });
};

module.exports = encounters;
