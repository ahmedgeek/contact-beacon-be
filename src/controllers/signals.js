const model = require('../models/signal');

exports.createSignal = data => {
  const { userID, latitude, longitude, accuracy } = data;
  return model.create({
    userID,
    accuracy,
    location: {
      type: 'Point',
      coordinates: [longitude, latitude],
    },
  });
};
