const jwt = require('jsonwebtoken');
const userModel = require('../models/user');
const logger = require('../helper/logger');

const auth = {};

auth.updateToken = updateObject => {
  return userModel
    .findOne({
      _id: updateObject.id,
    })
    .then(doc => {
      doc.deviceToken = updateObject.notificationToken;
      return doc.save();
    })
    .catch(reason => {
      logger.error(reason);
    });
};

auth.login = udid => {
  return userModel
    .findOne({
      udid: udid,
    })
    .then(result => {
      if (!result) {
        return userModel
          .create({
            udid: udid,
          })
          .then(userObject => {
            return new Promise((resolve, reject) => {
              jwt.sign(
                {
                  id: userObject._id,
                },
                process.env.WEB_TOKEN_SECRET,
                (err, token) => {
                  resolve({
                    ...userObject._doc,
                    token,
                  });
                },
              );
            });
          })
          .catch(error => {
            logger.error(error);
            return error;
          });
      } else {
        return new Promise((resolve, reject) => {
          jwt.sign(
            {
              id: result._id,
            },
            process.env.WEB_TOKEN_SECRET,
            (err, token) => {
              resolve({
                ...result._doc,
                token,
              });
            },
          );
        });
      }
    })
    .catch(error => {
      logger.error(error);
    });
};

module.exports = auth;
