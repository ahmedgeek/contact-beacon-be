const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  fullName: {
    type: String,
  },
  deviceToken: {
    type: String,
  },
  udid: {
    type: String,
    required: [true, 'Device ID is required'],
  },
  confirmedCase: {
    type: Boolean,
    default: false,
  },
});

const User = mongoose.model('User', userSchema, 'users');

module.exports = User;
