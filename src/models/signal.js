const mongoose = require('mongoose');

const signalSchema = new mongoose.Schema(
  {
    userID: {
      type: String,
      required: [true, 'User ID is required'],
    },
    location: {
      type: { type: String },
      coordinates: [],
    },
    accuracy: {
      type: Number,
      required: [false],
    },
  },
  {
    timestamps: true,
  },
);

const Signal = mongoose.model('Signal', signalSchema, 'signals');

module.exports = Signal;
