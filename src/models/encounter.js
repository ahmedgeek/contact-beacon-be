const mongoose = require('mongoose');

const encounterSchema = new mongoose.Schema(
  {
    confirmedCaseID: {
      type: String,
      required: [true, 'confirmed case id is required'],
    },
    encounteredID: {
      type: String,
      required: [true, 'encountered case id is required'],
    },
    location: {
      type: { type: String },
      coordinates: [],
    },
    encounterDate: {
      type: Date,
      required: [true, 'encounter date is required'],
    },
  },
  {
    timestamps: true,
  },
);

const Encounter = mongoose.model(
  'Encounter',
  encounterSchema,
  'encounters',
);

encounterSchema.index({ location: '2dsphere' });

module.exports = Encounter;
